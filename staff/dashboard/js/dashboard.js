let target = null;
let offset = 0;
let search = "";

function queryDatabase() {
  let pageInput = $('#page');
  $.ajax({
    url: '/staff/dashboard/components/DashboardTable.php',
    type: 'post',
    data: {
      'offset': offset,
      'search': search
    },
    success: function (data) {
      $('#table-wrapper').html(data);
      $('[data-tooltip="tooltip"]').tooltip();
      feather.replace();
      pageInput.val(offset / 25 + 1);
    }
  });
}

$(function () {
  target = $('#client-list');
  let backButton = $('#back');
  let pageInput = $('#page');
  let nextButton = $('#next');
  let timeoutID = 0;

  $('#search-bar').on('input', function () {
    search = this.value;
    window.clearTimeout(timeoutID);
    offset = 0;
    timeoutID = window.setTimeout(queryDatabase, 1000);
  });

  queryDatabase();

  // PAGE BUTTONS

  backButton.on('click', function () {
    if (offset > 0)
      offset -= 25;



    window.clearTimeout(timeoutID);
    timeoutID = window.setTimeout(queryDatabase, 200);
  });

  nextButton.on('click', function () {
    offset += 25;

    window.clearTimeout(timeoutID);
    timeoutID = window.setTimeout(queryDatabase, 200);
  });

  pageInput.on('input', function () {
    offset = (this.value - 1) * 25;
    window.clearTimeout(timeoutID);
    timeoutID = window.setTimeout(queryDatabase, 200);
  });

  feather.replace()
});