<?php
/**
 * StaffSidebar.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/24/18
 */

require_once "Link.php";

/**
 * @param $links Link[]
 */
function staffSidebar( $links ) {
  echo "
    <nav class='col-md-3 col-lg-2 d-none d-md-block bg-light sidebar'>
      <div class='sidebar-sticky p-0 m-0'>
        <ul class='nav flex-column list-group list-group-flush'>";
  foreach ( $links as $link ) {
    $link->sideRender();
  }
  echo "</ul>";

  /*
  <h6 class='sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted'>
    <span>Saved reports</span>
    <a class='d-flex align-items-center text-muted' href='#'>
      <span data-feather='plus-circle'></span>
    </a>
  </h6>
  <ul class='nav flex-column mb-2'>
    <li class='nav-item'>
      <a class='nav-link' href='#'>
        <span data-feather='file-text'></span>
        Current month
      </a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='#'>
        <span data-feather='file-text'></span>
        Last quarter
      </a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='#'>
        <span data-feather='file-text'></span>
        Social engagement
      </a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='#'>
        <span data-feather='file-text'></span>
        Year-end sale
      </a>
    </li>
  </ul>
  */

  echo "
      </div>
    </nav>
  ";
}