<?php
session_start();
/**
 * Created by PhpStorm.
 * User: Joseph Bethards
 * Date: 2/18/2018
 * Time: 6:07 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Emergency Contact</title>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

</head>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SKCAC Emergency Contact</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--  <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">-->
    <link rel="stylesheet" href="../css/dashboard.css">
</head>
<style>
    .header{
        margin-left: 2%;
        margin-top:1%;

    }
    .nav1{
        padding:0px;
    }
    .navitem2{
        float: right;
    }
    .marg{
        margin-right: 1%;
        margin-left: 1%;
    }
</style>
<body>
<nav class="navbar navbar-dark bg-secondary justify-content-between  flex-nowrap nav1 flex-row">
    <div class="bg-dark">
        <a href="UserLanding.php" class="navbar-brand float-left">  <img src="/staff/resources/skcac_header-3.png" height="15%" width="14%">
            SKCAC Industrusties</a>
    </div>
    <div class="navitem2">
        <a class="navbar-brand float-right" href="/staff/signout/index.php">Sign Out</a></li>
        <a href="medications.php" class="navbar-brand float-right">Medical Information</a>

        <a href="updateContact1.1.php" class="navbar-brand float-right">Emergency Contacts</a>

    </div>
</nav>

        <main>
            <div class="container-fluid">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">



            <?php
            echo"<h1>$_SESSION[name]</h1>";
            echo"</div>";
            echo "</div>";
            echo"<h1 class='j h2 marg'>Emergency Information</h1>";
            echo"</div>";
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {

                require("/home/jacadeve/public_html/Joseph/db/db.php");
                if (empty($_POST['first_name'])) {
                    $errors[] = 'You forgot to enter your first name.';
                    echo "<p>Please enter a emergency first name</p>";

                } else {
                    $first_name = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
                }
                if (empty($_POST['last_name'])) {
                    $errors[] = 'You forgot to enter a last name.';
                    echo "<p>Please enter a emergency last name</p>";
                } else {
                    $last_name = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
                }
                // Check for a last name:
                if (empty($_POST['phone_number']) ) {
                    $errors[] = 'You forgot to enter a phone number.';
                    echo "<p>Please enter a emergency phone number.</p>";
                } else {

                    $tempPhone = preg_replace('/[^0-9]{10,11}/', '', $_POST['phone']);
                    //checks is it valid length of the phone number, if yes, it assigns the variable
                    if (strlen($tempPhone) >= 10 || strlen($tempPhone) <= 11) {
                        $phone = $tempPhone;
                    } else {
                        $errors[] = "Please enter valid phone number with 10 or 11 numbers.";
                    }


                    $phone_number = mysqli_real_escape_string($dbc, trim($_POST['phone_number']));
                    $phone_number=str_replace("-","",$phone_number);
                }
                if (empty($_POST['alternative_phone_number'])) {
                    $alternative_phone_number=null;
                } else {

                    $alternative_phone_number = mysqli_real_escape_string($dbc, trim($_POST['alternative_phone_number']));
                    $alternative_phone_number=str_replace("-","",$alternative_phone_number);
                }


                if (empty($errors)) { // If everything's OK.
                    // Register the user in the database...
                    // Make the query:

                    $q = "INSERT INTO Clients_Emergency_Contacts (client_id,first_name, last_name, phone, alternate_phone) VALUES ($_SESSION[clientid],'$first_name','$last_name',$phone_number,$alternative_phone_number);";
                    $r = @mysqli_query($dbc, $q); // Run the query.
                    if ($r) { // If it ran OK.
                        // Print a message:
                        $full_name=$first_name.' '.$last_name;
                        echo "<p>Thank You, Your information has been posted</p>";
                        echo "<p> Emergency Contact Name: $full_name</p>";
                        echo "<p> Phone Number: $phone_number";
                        echo "<p> Alternative Phone: $alternative_phone_number";
                        echo "<p> Conitinue or add another contact</p>";
                        echo "<form method='post' action='UserLanding.php'><button type='submit' class='btn btn-primary'>Home</button></form>";

                    } else { // If it did not run OK.
                        // Public message:
                        echo '<h1>System Error</h1>
            <p class="error">You could not be registered due to a system error. We apologize for any inconvenience.</p>';
                        // Debugging message:
                       #echo '<p>' . mysqli_error($dbc) . '<br><br>Query: ' . $q . '</p>';
                    } // End of if ($r) IF.
                    mysqli_close($dbc); // Close the database connection.
                    // Include the footer and quit the script:
                }

            }

            ?>
    <form action="EmergenyContact.php" method="post">
    <div class="form-row">
            <div class="form-group col-lg-3 marg ">
                <label for="first_name">Emergency First</label>
                <input type="text" name="first_name" class="form-control" id="first_name" value="<?php echo $first_name;?>">
            </div>
            <div class="form-group col-lg-3 marg">
                <label for="last_name">Emergency Last</label>
                <input type="text" name="last_name" class="form-control" id="last_name" value="<?php echo $last_name;?>">
            </div>
    </div>
        <div class="form-row">
        <div class="form-group col-lg-3 marg">
            <label for="phone_number">Emergency Phone Number</label>
            <input type="text" name="phone_number"  class="form-control"id="phone_number" value="<?php echo $phone_number;?>">
        </div>
        <div class="form-group col-lg-3 marg">
            <label for="alternative_phone_number">Alternative Phone Number</label>
            <input type="text" name="alternative_phone_number"  class="form-control" id="alternative_phone_number value="<?php echo $alternative_phone_number?>" >
        </div>
        </div>
        <button type="submit" class="btn btn-primary marg">Submit Information</button>
    </form>


        </main>
    </div>


<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>
<script src="/home/jacadeve/public_html/register/register.js"></script>
<script src="/home/jacadeve/public_html/register/register-backup.js"></script>
<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
<!--<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>-->
    <script src="/register/register.js"></script>
    <script src="/register/register-backup.js"></script>

</body>
</html>