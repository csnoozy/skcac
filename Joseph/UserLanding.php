<?php
session_start();
/**
 * Created by PhpStorm.
 * User: Joseph Bethards
 * Date: 2/21/2018
 * Time: 7:19 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Emergency Contact</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--  <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">-->
    <link rel="stylesheet" href="/css/navbarStyle.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">

</head>
<style>
  .header{
        margin-right: 2%;
        margin-left: 2%;
        margin-top:1%;

    }
    .nav1{
        padding:0px;
    }
    .navitem2{
        float: right;
    }
    .font{
        font-family: 'Vollkron', serif;
        font-size: 100px;
    }
</style>
<body>
<div class="bg-dark">
<!--<div class="row">
    <div class="header">

        <img src="/staff/resources/skcac_header-3.png" height="90%" width="100%">
    </div>
        <h1 class="display-3 text-white"> SKCAC Industries</h1>
</div>-->

</div>
<nav class="navbar navbar-dark bg-secondary font justify-content-between flex-nowrap nav1 flex-row">
    <div class="bg-dark">
    <a href="UserLanding.php" class="navbar-brand float-left">  <img src="/staff/resources/skcac_header-3.png" height="15%" width="14%">
        SKCAC Industrusties</a>
    </div>
    <div class="navitem2">
        <a class="navbar-brand float-right" href="/staff/signout/index.php">Sign Out</a></li>
    <a href="medications.php" class="navbar-brand float-right">Medical Information</a>

    <a href="updateContact1.1.php" class="navbar-brand float-right">Emergency Contacts</a>

    </div>
</nav>
        <main role="main">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <?php
                require("/home/jacadeve/public_html/Joseph/db/db.php");
                $q = "SELECT Account.first_name, Account.last_name,Account.email,Account.account_id FROM Account Inner Join Clients on Clients.account_id=Account.account_id WHERE email ='$_SESSION[email]';";
                $r = @mysqli_query($dbc, $q);
                while($rs = mysqli_fetch_assoc($r)){
                    $first_name = $rs['first_name'];
                    $last_name = $rs['last_name'];
                    $acnt=$rs['account_id'];
                }
                if($r){

                }
                else{
                    $q = "SELECT Account.first_name, Account.last_name,Account.email,Account.account_id FROM Account Inner Join Clients on Clients.account_id=Account.account_id WHERE email ='$_SESSION[email2]';";
                    $r = @mysqli_query($dbc, $q);
                    while($rs = mysqli_fetch_assoc($r)){
                        $first_name = $rs['first_name'];
                        $last_name = $rs['last_name'];
                        $acnt=$rs['account_id'];
                    }

                }
                $joesQ= "SELECT client_id FROM Clients WHERE account_id=$acnt";
                $p = @mysqli_query($dbc, $joesQ);
                while($e = mysqli_fetch_assoc($p)){
                    $clie=$e["client_id"];
                }
                // End of if ($r) IF.// End of if ($r) IF.

                $_SESSION['clientid']=$clie;
                $_SESSION['name']=$first_name." ". $last_name;
                $_SESSION['accountID']=$acnt;
                echo "
                    <div class='container-fluid'>
                    <h1 class='display-3'>$_SESSION[name]</h1>
                    </div>";
                ?>


            </div>
            <a class="nav-link active" href="EmergenyContact.php">

                Please add Emergency Contacts <span class="sr-only"></span>
            </a>

    </main>
<?php
include ("/home/jacadeve/public_html/register/footer.html");
?>


    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
        feather.replace()
    </script>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
    <!--<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>-->

</body>
</html>
