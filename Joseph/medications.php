<?php
/**
 * Created by PhpStorm.
 * User: Joseph Bethards
 * Date: 3/2/2018
 * Time: 11:18 AM
 */
$client_id = $_SESSION['clientid'];
require '/home/jacadeve/public_html/common/index.php';
require_once '/home/jacadeve/public_html/common/Client.php';
require_once '/home/jacadeve/public_html/common/Account.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $client_id = $_SESSION['clientid'];
    require("/home/jacadeve/public_html/Joseph/db/db.php");
    $newmed = $_POST['medication'];
    $newdosage = $_POST['dosage'];
    $newamount = $_POST['frequency'];
    $newtime = $_POST['time_taken'];
    $q="INSERT INTO Clients_Medications(client_id, medication, dosage, frequency, time_taken, active)
    VALUES ($client_id,'$newmed','$newdosage','$newamount','$newtime',1)";
    $r = @mysqli_query($dbc, $q);
    if ($r) { // If it ran OK.
        // Print a message:

    } else { // If it did not run OK.
        // Public message:
        echo '<h1>System Error</h1>
			<p class="error">You could not be registered due to a system error. We apologize for any inconvenience.</p>';
        // Debugging message:
        echo '<p>' . mysqli_error($dbc) . '<br><br>Query: ' . $q . '</p>';
    } // End of if ($r) IF.
    mysqli_close($dbc); // Close the database connection.
// Include the footer and qu
}
$client_id = $_SESSION['clientid'];
$participant = Participant::query_from_client_id( $client_id );

$first_name = htmlentities( $participant->getFirstName(), ENT_QUOTES, 'UTF-8' );
$last_name = htmlentities( $participant->getLastName(), ENT_QUOTES, 'UTF-8' );
$middle_name = htmlentities( $participant->getMiddleName(), ENT_QUOTES, 'UTF-8' );
$full_name = htmlentities( $participant->getFirstName() . ' ' . $participant->getLastName(), ENT_QUOTES, 'UTF-8' );
$email = htmlentities( $participant->getEmail(), ENT_QUOTES, 'UTF-8' );
#$phone = htmlentities( $participant->getPhone(), ENT_QUOTES, 'UTF-8' );
$address = htmlentities( $participant->getAddress(), ENT_QUOTES, 'UTF-8' );
$address_city = htmlentities( $participant->getAddressCity(), ENT_QUOTES, 'UTF-8' );
$address_zip = htmlentities( $participant->getAddressZip(), ENT_QUOTES, 'UTF-8' );
$address_state = htmlentities( $participant->getAddressState(), ENT_QUOTES, 'UTF-8' );
$last_update = htmlentities( $participant->getLastUpdate(), ENT_QUOTES, 'UTF-8' );
$active_client = htmlentities( $participant->isActiveClient(), ENT_QUOTES, 'UTF-8' );
$medications = $participant->get_medications();
function medication_table_row( array $columns, int $id, string $type ) {
    echo "<tr>";
    foreach ( $columns as $col ) {
        echo "<td class='p-0 m-0 align-middle'>$col</td>";
    }
    echo " <td class='text-right p-0 m-0'>
      <button class='btn btn-secondary btn-sm dropdown-toggle w-100'
      type='button' 
      data-toggle='dropdown' 
      aria-haspopup='true' 
      aria-expanded='false'
      form='javascript.void(0)'>
        <span class='d-none d-sm-inline'>Menu</span><span data-feather='menu'>...</span>
      </button>
      <div class='dropdown-menu p-0 m-0'>
        <div class='btn-group-vertical p-0 m-0 d-none d-xl-block'>
          <button class='btn btn-sm btn-secondary' 
          data-toggle='modal' 
          data-target='#edit-$type' 
          data-id='" . $id . "' 
          data-tooltip='tooltip' 
          data-placement='top' 
          form='javascript.void(0)'
          title='Edit $type'>
            <span class='d-none d-xl-inline'>Edit </span><span data-feather='edit'>Edit</span>
          </button>
          <button class='btn btn-sm btn-danger' 
          data-tooltip='tooltip' 
          data-placement='top' 
          form='javascript.void(0)'
          title='Remove $type'>
            <span class='d-none d-xl-inline'>Remove </span><span data-feather='delete'>X</span>
          </button>
        </div>
        <div class='btn-group p-0 m-0 d-xl-none'>
          <button class='btn btn btn-secondary' 
          data-toggle='modal' 
          data-target='#edit-$type' 
          data-id='" . $id . "' 
          data-tooltip='tooltip' 
          data-placement='top' 
          form='javascript.void(0)'
          title='Edit $type'>
          <span class='d-none d-xl-inline'>Edit </span><span data-feather='edit'>Edit</span>
          </button>
          <button class='btn btn btn-danger' 
          data-tooltip='tooltip' 
          data-placement='top' 
          form='javascript.void(0)'
          title='Remove $type'>
            <span class='d-none d-xl-inline'>Remove </span><span data-feather='delete'>X</span>
          </button>
        </div>
      </div>
    </td>              
  </tr>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SKCAC Emergency Contact</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--  <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">-->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<style>
    .header{
        margin-left: 2%;
        margin-top:1%;

    }
    .nav1{
        padding:0px;
    }
    .navitem2{
        float: right;
    }
</style>
<body>
<nav class="navbar navbar-dark bg-secondary justify-content-between  flex-nowrap nav1 flex-row">
    <div class="bg-dark">
        <a href="UserLanding.php" class="navbar-brand float-left">  <img src="/staff/resources/skcac_header-3.png" height="15%" width="14%">
            SKCAC Industrusties</a>
    </div>
    <div class="navitem2">
        <a class="navbar-brand float-right" href="/staff/signout/index.php">Sign Out</a></li>
        <a href="medications.php" class="navbar-brand float-right">Medical Information</a>

        <a href="updateContact1.1.php" class="navbar-brand float-right">Emergency Contacts</a>

    </div>
</nav>

<main>
    <div class="container-fluid">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1><?php echo"$_SESSION[name]"?></h1>
        </div>
        <h1>Medications</h1>
    </div>

    <!--<form action='/api/client/<?php echo $client_id ?>/medication'
          id='medication-add-form'
          method='post'>-->
    <form action="medications.php" id="medication" method="post">
        <table class="table table-hover table-striped table-sm table-light m-0">
            <thead class="bg-dark text-light">
            <tr>
                <th>Medication</th>
                <th>Dosage</th>
                <th>Frequency</th>
                <th>Time Taken</th>
                <th class="text-right fit">Controls</th>
            </tr>
            </thead>
            <tbody class='shadow-inset'>
            <?php
            foreach ( $medications as $medication ) {
                medication_table_row( array( $medication->getMedication(),
                    $medication->getDosage(),
                    $medication->getFrequency(),
                    $medication->getTimeTaken()
                ), $medication->getId(), "medication" );
            }
            ?>
            <tr class='bg-secondary'>
                <td class='p-0 m-0'>
                    <div class='input-group input-group-sm'>
                        <input class='form-control'
                               id='medication-add-input-medication'
                               name='medication'
                               type='text'
                               placeholder='Medication'
                               aria-label='Medication'
                               required>
                        <div class='input-group-append'>
                      <span class='input-group-text d-none d-lg-inline'>
                        <small>
                          <i data-feather='circle'></i>
                        </small>
                      </span>
                        </div>
                    </div>
                </td>
                <td class='p-0 m-0'>
                    <div class='input-group input-group-sm'>
                        <input class='form-control'
                               id='medication-add-input-dosage'
                               name='dosage'
                               type='text'
                               placeholder='Dosage'
                               aria-label='Dosage'
                               required>
                        <div class='input-group-append'>
                      <span class='input-group-text d-none d-lg-inline'>
                        <small>
                          <i data-feather='circle'></i>
                        </small>
                      </span>
                        </div>
                    </div>
                </td>
                <td class='p-0 m-0'>
                    <div class='input-group input-group-sm'>
                        <input class='form-control'
                               id='medication-add-input-frequency'
                               name='frequency'
                               type='text'
                               placeholder='Frequency'
                               aria-label='Frequency'
                               required>
                        <div class='input-group-append'>
                      <span class='input-group-text d-none d-lg-inline'>
                        <small>
                          <i data-feather='circle'></i>
                        </small>
                      </span>
                        </div>
                    </div>
                </td>
                <td class='p-0 m-0'>
                    <div class='input-group input-group-sm'>
                        <input class='form-control'
                               id='medication-add-input-time-taken'
                               name='time_taken'
                               type='text'
                               placeholder='Time Taken'
                               aria-label='Time Taken'
                               required>
                        <div class='input-group-append'>
                      <span class='input-group-text d-none d-lg-inline'>
                        <i data-feather='circle'></i>
                      </span>
                        </div>
                    </div>
                </td>
                <td class='text-right p-0 m-0'>
                    <input type="submit" value="Add Medication" class="btn btn-sm btn-success w-100">
                </td>
            </tr>
            </tbody>
            <tfoot class='bg-secondary text-light'>
            <tr>
                <th>
                    <small>Medication name and information</small>
                </th>
                <th>
                    <small>Dosage in mg</small>
                </th>
                <th>
                    <small>Frequency Taken</small>
                </th>
                <th>
                    <small>Taken At</small>
                </th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </form>
    <?php
    require '/home/jacadeve/public_html/Joseph/modals/edit_medication.html';

    ?>
    </div>
    </div>
</main>
</body>
<script>
    $(function () {
        $('[data-tooltip="tooltip"]').tooltip()
    });

    $('.input-group').find('input').on('input', function () {
        let input = $(this);
        if (input.prop('required')) {
            let span = input.parent().find('span');
            if (input.val() === "") {
                span.removeClass('bg-success');
                span.addClass('bg-danger');
                span.html("<i data-feather='x-circle'>X</i>");
                feather.replace();
                input.addClass('is-invalid');
            } else {
                span.removeClass('bg-danger');
                span.addClass('bg-success');
                span.html("<i data-feather='check-circle'>:)</i>");
                feather.replace();
                input.removeClass('is-invalid');
            }
        }
    });
</html>

