<?php
/**
 * EmergencyContact.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/17/18
 */

require_once 'DBTable.php';
require_once 'DB.php';

class EmergencyContact extends DBTable {

  # GET QUERIES
  const QUERY_FROM_CLIENT = "SELECT * FROM Clients_Emergency_Contacts WHERE client_id = :client_id;";
  const QUERY_FROM_ID = "SELECT * FROM Clients_Emergency_Contacts WHERE emergency_contact_id = :id LIMIT 1;";

  const QUERY_UPDATE = "UPDATE Clients_Emergency_Contacts SET
    first_name = :first_name, 
    last_name = :last_name, 
    phone = :phone, 
    alternate_phone = :alternate_phone
  WHERE emergency_contact_id = :id";

  protected $emergency_contact_id,
    $client_id,
    $first_name,
    $last_name,
    $phone,
    $alternate_phone,
    $active_contact;


  /**
   * @param int $client_id
   * @return EmergencyContact[]
   */
  public static function query_from_client( int $client_id ): array {
    return self::queryAll( self::QUERY_FROM_CLIENT, array( ':client_id' => $client_id ) );
  }


  /**
   * @param int $id
   * @return EmergencyContact|bool
   */
  public static function query_from_id( int $id ) {
    return self::queryOne( self::QUERY_FROM_ID, array( ':id' => $id ) );
  }

//  function insert(): bool {
//  }
//
  public function update(): bool {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':first_name', $this->first_name, PDO::PARAM_STR );
      $stmt->bindParam( ':last_name', $this->last_name, PDO::PARAM_STR );
      $stmt->bindParam( ':phone', $this->phone, PDO::PARAM_INT );
      $stmt->bindParam( ':alternate_phone', $this->alternate_phone, PDO::PARAM_INT );
      $stmt->bindParam( ':id', $this->emergency_contact_id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return int
   */
  public function getEmergencyContactId(): int {
    return $this->emergency_contact_id;
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @param int $client_id
   */
  public function setClientId( int $client_id ): void {
    $this->client_id = $client_id;
  }

  /**
   * @return string
   */
  public function getFirstName(): string {
    return $this->first_name;
  }

  /**
   * @param string $first_name
   */
  public function setFirstName( string $first_name ): void {
    $this->first_name = $first_name;
  }

  /**
   * @return string
   */
  public function getLastName(): string {
    return $this->last_name;
  }

  /**
   * @param string $last_name
   */
  public function setLastName( string $last_name ): void {
    $this->last_name = $last_name;
  }

  /**
   * @return int
   */
  public function getPhone(): int {
    return $this->phone;
  }

  /**
   * @param int $phone
   */
  public function setPhone( int $phone ): void {
    $this->phone = $phone;
  }

  /**
   * @return int
   */
  public function getAlternatePhone(): int {
    return $this->alternate_phone;
  }

  /**
   * @param int $alternate_phone
   */
  public function setAlternatePhone( int $alternate_phone ): void {
    $this->alternate_phone = $alternate_phone;
  }

  /**
   * @return bool
   */
  public function isActiveContact(): bool {
    return $this->active_contact;
  }

  /**
   * @param bool $active_contact
   */
  public function setActiveContact( bool $active_contact ): void {
    $this->active_contact = $active_contact;
  }
}