<?php
/**
 * Account.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/17/18
 */

require_once 'DBTable.php';
require_once 'Client.php';

class Account extends DBTable {

  const QUERY_ALL = "SELECT * FROM Account;";
  const QUERY_FROM_EMAIL = "SELECT * FROM Account WHERE email = :email LIMIT 1;";
  const QUERY_FROM_ID = "SELECT * FROM Account WHERE account_id = :account_id LIMIT 1;";

  /** Update a Account's data. */
  const QUERY_UPDATE = "UPDATE Account SET 
  email = :email, 
  first_name = :first_name,
  last_name = :last_name,
  password = :password, 
  is_staff = :is_staff,
  is_admin = :is_admin,
  account_created = :account_created
  WHERE account_id = :account_id;";

  protected $account_id;
  protected $email;
  protected $first_name;
  protected $last_name;
  protected $salt;
  protected $password;
  protected $is_staff;
  protected $is_admin;
  protected $account_created;

  /**
   * @param string $email
   * @return Account|bool
   */
  public static function query_from_email( string $email ): Account {
    return self::queryOne( self::QUERY_FROM_EMAIL, array( ':email' => $email ) );
  }

  /**
   * @param int $account_id
   * @return Account|bool
   */
  public static function query_from_id( int $account_id ): Account {
    return self::queryOne( self::QUERY_FROM_ID, array( ':account_id' => $account_id ) );
  }

  /**
   * @return Account[]
   */
  public static function query_all() {
    return self::queryAll( self::QUERY_ALL );
  }

  public function update() {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':email', $this->email, PDO::PARAM_STR );
      $stmt->bindParam( ':first_name', $this->first_name, PDO::PARAM_STR );
      $stmt->bindParam( ':last_name', $this->last_name, PDO::PARAM_STR );
      $stmt->bindParam( ':password', $this->password, PDO::PARAM_STR );
      $stmt->bindParam( ':is_staff', $this->is_staff, PDO::PARAM_BOOL );
      $stmt->bindParam( ':is_admin', $this->is_admin, PDO::PARAM_BOOL );
      $stmt->bindParam( ':account_created', $this->account_created, PDO::PARAM_STR );
      $stmt->bindParam( ':account_id', $this->account_id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return int
   */
  public function getAccountId(): int {
    return $this->account_id;
  }

  /**
   * @return string
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail( string $email ): void {
    $this->email = $email;
  }

  /**
   * @return string
   */
  public function getFirstName(): string {
    return $this->first_name;
  }

  /**
   * @param string $first_name
   */
  public function setFirstName( string $first_name ): void {
    $this->first_name = $first_name;
  }

  /**
   * @return string
   */
  public function getLastName(): string {
    return $this->last_name;
  }

  /**
   * @param string $last_name
   */
  public function setLastName( string $last_name ): void {
    $this->last_name = $last_name;
  }

  /**
   * @return string
   */
  public function getSalt(): string {
    return $this->salt;
  }

  /**
   * @return string
   */
  public function getPassword(): string {
    return $this->password;
  }

  /**
   * @param string $password
   */
  public function setPassword( string $password ): void {
    $this->password = $password;
  }

  /**
   * @return bool
   */
  public function isStaff(): bool {
    return $this->is_staff;
  }

  /**
   * @param bool $is_staff
   */
  public function setIsStaff( bool $is_staff ): void {
    $this->is_staff = $is_staff;
  }

  /**
   * @return bool
   */
  public function isAdmin(): bool {
    return $this->is_admin;
  }

  /**
   * @param bool $is_admin
   */
  public function setIsAdmin( bool $is_admin ): void {
    $this->is_admin = $is_admin;
  }

  /**
   * @return string
   */
  public function getAccountCreated(): string {
    return $this->account_created;
  }

  /**
   * @param string $account_created
   */
  public function setAccountCreated( string $account_created ): void {
    $this->account_created = $account_created;
  }
}

class Participant extends Account {
  const QUERY_FROM_ACCOUNT_ID = "SELECT * FROM Clients 
    INNER JOIN Account ON Clients.account_id = Account.account_id WHERE Account.account_id = :account_id LIMIT 1;";

  const QUERY_FROM_CLIENT_ID = "SELECT * FROM Clients 
    INNER JOIN Account ON Clients.account_id = Account.account_id WHERE client_id = :client_id LIMIT 1;";

  const QUERY_FROM_EMAIL = "SELECT * FROM Clients 
    INNER JOIN Account ON Clients.account_id = Account.account_id WHERE email = :email LIMIT 1;";

  const QUERY_SEARCH = "SELECT * FROM Clients 
    INNER JOIN Account ON Clients.account_id = Account.account_id 
    WHERE
    email LIKE CONCAT('%', :search, '%') OR
    first_name LIKE CONCAT(:search, '%') OR
    last_name LIKE CONCAT(:search, '%') OR
    phone LIKE CONCAT('%', :search, '%')
    LIMIT 25 OFFSET :off;";

  const QUERY_DATA_TABLE = "SELECT client_id, CONCAT(last_name, ', ', first_name) as name, email, phone, last_update FROM Clients
    INNER JOIN Account ON Clients.account_id = Account.account_id;";

  protected $client_id,
    $middle_name,
    $phone,
    $address,
    $address_city,
    $address_zip,
    $address_state,
    $last_update,
    $active_client;


  /**
   * @param int $offset
   * @param string $search
   * @return Participant[]
   */
  public static function query_all( int $offset = 0, string $search = "" ) {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_SEARCH );
      $stmt->bindParam( ':search', $search, PDO::PARAM_STR );
      $stmt->bindParam( ':off', $offset, PDO::PARAM_INT );
      $stmt->execute();
      $data = $stmt->fetchAll( PDO::FETCH_CLASS, 'Participant' );
      return $data;
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . '\n' . $ex->getTraceAsString() );
    }
  }

  /**
   * @return Participant[]
   */
  public static function query_for_data_table(): array {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_DATA_TABLE );
      $stmt->execute();
      $data = $stmt->fetchAll( PDO::FETCH_CLASS, 'Participant' );
      return $data;
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . '\n' . $ex->getTraceAsString() );
    }
  }

  /**
   * @param int $account_id
   * @return Participant|bool
   */
  public static function query_from_account_id( int $account_id ) {
    return self::queryOne( self::QUERY_FROM_ACCOUNT_ID, array( ':account_id' => $account_id ) );
  }

  /**
   * @param int $client_id
   * @return Participant|bool
   */
  public static function query_from_client_id( int $client_id ) {
    return self::queryOne( self::QUERY_FROM_CLIENT_ID, array( ':client_id' => $client_id ) );
  }

  /**
   * @param string $email
   * @return Participant|bool
   */
  public static function query_from_account_email( string $email ) {
    return self::queryOne( self::QUERY_FROM_EMAIL, array( ':email' => $email ) );
  }

  public function update(): bool {
    if ( $this->get_client()->update() )
      return $this->get_account()->update();
    return false;
  }

  /**
   * Create a client object based of this Participants current fields.
   *
   * @return Client
   */
  public function get_client() {
    $client = Client::query_from_id( $this->getClientId() );
    $client->setMiddleName( $this->getMiddleName() );
    $client->setPhone( $this->getPhone() );
    $client->setAddress( $this->getAddress() );
    $client->setAddressCity( $this->getAddressCity() );
    $client->setAddressZip( $this->getAddressZip() );
    $client->setAddressState( $this->getAddressState() );
    $client->setLastUpdate( $this->getLastUpdate() );
    $client->setActiveClient( $this->isActiveClient() );
    return $client;
  }

  /**
   * Create a account object based of this Participants current fields.
   *
   * @return Account
   */
  public function get_account() {
    $account = Account::query_from_id( $this->getAccountId() );
    $account->setEmail( $this->getEmail() );
    $account->setFirstName( $this->getFirstName() );
    $account->setLastName( $this->getLastName() );
    $account->setPassword( $this->getPassword() );
    $account->setIsStaff( $this->isStaff() );
    $account->setIsAdmin( $this->isAdmin() );
    $account->setAccountCreated( $this->getAccountCreated() );
    return $account;
  }

  /**
   * @return Contact[] This clients contacts.
   */
  public function get_contacts(): array {
    return Contact::query_from_client( $this->client_id );
  }


  /**
   * @param int $id Specific Contacts ID.
   * @return Contact Specified Contact.
   */
  public function get_contact( int $id ): Contact {
    return Contact::query_from_id( $id );
  }

  /**
   * @return EmergencyContact[] This Clients Emergency Contacts.
   */
  public function get_emergency_contacts(): array {
    return EmergencyContact::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Emergency Contacts ID.
   * @return EmergencyContact Specified Emergency Contact.
   */
  public function get_emergency_contact( int $id ): EmergencyContact {
    return EmergencyContact::query_from_id( $id );
  }

  /**
   * @return MedicalAlert[] Clients Medical Alerts.
   */
  public function get_medical_alerts(): array {
    return MedicalAlert::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Alert ID.
   * @return MedicalAlert Specified Alert.
   */
  public function get_medical_alert( int $id ): MedicalAlert {
    return MedicalAlert::query_from_id( $id );
  }

  /**
   * @return PhysicalLimitation[] Clients Physical Limitations.
   */
  public function get_physical_limitations(): array {
    return PhysicalLimitation::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Limitations ID.
   * @return PhysicalLimitation Specified Limitation.
   */
  public function get_physical_limitation( int $id ): PhysicalLimitation {
    return PhysicalLimitation::query_from_id( $id );
  }

  /**
   * @return DietRestriction[] Clients Diet Restrictions.
   */
  public function get_diet_restrictions(): array {
    return DietRestriction::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Restrictions ID.
   * @return DietRestriction Specified Restriction.
   */
  public function get_diet_restriction( int $id ): DietRestriction {
    return DietRestriction::query_from_id( $id );
  }

  /**
   * @return Medication[] Clients Medications.
   */
  public function get_medications(): array {
    return Medication::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Medications ID.
   * @return Medication Specified Medication.
   */
  public function get_medication( int $id ): Medication {
    return Medication::query_from_id( $id );
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @return string
   */
  public function getMiddleName(): string {
    return $this->middle_name;
  }

  /**
   * @param string $middle_name
   */
  public function setMiddleName( string $middle_name ): void {
    $this->middle_name = $middle_name;
  }

  /**
   * @return int
   */
  public function getPhone(): int {
    return $this->phone;
  }

  /**
   * @param int $phone
   */
  public function setPhone( int $phone ): void {
    $this->phone = $phone;
  }

  /**
   * @return string
   */
  public function getAddress(): string {
    return $this->address;
  }

  /**
   * @param string $address
   */
  public function setAddress( string $address ): void {
    $this->address = $address;
  }

  /**
   * @return string
   */
  public function getAddressCity(): string {
    return $this->address_city;
  }

  /**
   * @param string $address_city
   */
  public function setAddressCity( string $address_city ): void {
    $this->address_city = $address_city;
  }

  /**
   * @return string
   */
  public function getAddressZip(): string {
    return $this->address_zip;
  }

  /**
   * @param string $address_zip
   */
  public function setAddressZip( string $address_zip ): void {
    $this->address_zip = $address_zip;
  }

  /**
   * @return string
   */
  public function getAddressState(): string {
    return $this->address_state;
  }

  /**
   * @param string $address_state
   */
  public function setAddressState( string $address_state ): void {
    $this->address_state = $address_state;
  }

  /**
   * @return string
   */
  public function getLastUpdate(): string {
    return $this->last_update;
  }

  /**
   * @param string $last_update
   */
  public function setLastUpdate( string $last_update ): void {
    $this->last_update = $last_update;
  }

  /**
   * @return bool
   */
  public function isActiveClient(): bool {
    return $this->active_client;
  }

  /**
   * @param bool $active_client
   */
  public function setActiveClient( bool $active_client ): void {
    $this->active_client = $active_client;
  }


}