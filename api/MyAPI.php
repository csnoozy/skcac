<?php
/**
 * MyAPI.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/16/18
 */

require_once '../common/index.php';
require_once '../common/Account.php';
require_once '../common/Client.php';
require_once 'API.php';

class MyAPI extends API {

  public function __construct( $request ) {
    try {
      parent::__construct( $request );
    } catch ( Exception $e ) {
      die( $e->getMessage() . PHP_EOL . $e->getTraceAsString() );
    }

    // Authentication Check
    verify_session( true );
  }

  protected function client() {
    if ( $this->method == 'GET' ) {
      if ( empty( $this->args ) || $this->args[ 0 ] <= 0 )
        return "Missing or Invalid Client ID";
      $id = $this->args[ 0 ];  // Request Client ID
      $client = Client::query_from_id( $id ); // Get Client from database

      if ( count( $this->args ) == 1 ) { // return the client
        return $client->decode();
      } else if ( count( $this->args ) == 2 ) { // return array of client additional info
        switch ( $this->args[ 1 ] ) {
          case 'contacts':
            $data = $client->get_contacts();
            break;
          case 'diet_restrictions':
            $data = $client->get_diet_restrictions();
            break;
          case 'emergency_contacts':
            $data = $client->get_emergency_contacts();
            break;
          case 'medical_alerts':
            $data = $client->get_medical_alerts();
            break;
          case 'medications':
            $data = $client->get_medications();
            break;
          case 'physical_limitations':
            $data = $client->get_physical_limitations();
            break;
          default:
            return "Invalid argument";
        }
        return array_map( function ( DBTable $table ) {
          return $table->decode();
        }, $data );
      } else if ( count( $this->args ) == 3 ) { // return client addition info by id
        $id = $this->args[ 2 ];
        $data = null;
        switch ( $this->args[ 1 ] ) {
          case 'contact':
            $data = $client->get_contact( $id )->decode();
            break;
          case 'diet_restriction':
            $data = $client->get_diet_restriction( $id )->decode();
            break;
          case 'emergency_contact':
            $data = $client->get_emergency_contact( $id )->decode();
            break;
          case 'medical_alert':
            $data = $client->get_medical_alert( $id )->decode();
            break;
          case 'medication':
            $data = $client->get_medication( $id )->decode();
            break;
          case 'physical_limitation':
            $data = $client->get_physical_limitation( $id )->decode();
            break;
          default:
            return "Invalid argument";
        }
        return $data;
      } else {
        return "Invalid number of arguments";
      }
    } else if ( $this->method == 'POST' ) {
      if ( empty( $this->args ) || $this->args[ 0 ] <= 0 )
        return "Missing or Invalid Client ID";
      $id = $this->args[ 0 ];  // Request Client ID
      $participant = Participant::query_from_client_id( $id ); // Get Participant from database

      if ( count( $this->args ) === 1 ) { // Participant update
        $participant->setFirstName( $this->request[ 'first_name' ] );
        $participant->setMiddleName( $this->request[ 'middle_name' ] );
        $participant->setLastName( $this->request[ 'last_name' ] );
        $participant->setPhone( $this->request[ 'phone' ] );
        $participant->setEmail( $this->request[ 'email' ] );
        $participant->setAddress( $this->request[ 'address' ] );
        $participant->setAddressCity( $this->request[ 'address_city' ] );
        $participant->setAddressZip( $this->request[ 'address_zip' ] );
        $participant->setAddressState( $this->request[ 'address_state' ] );

        $_SESSION[ 'update_status' ] = $participant->update();

        header( "Location: /staff/dashboard/client/?client_id=" . $participant->getClientId() );
        die();
      } else if ( count( $this->args ) === 2 ) { // Insert new Data
        return "New Items not supported yet!";
      } else if ( count( $this->args ) === 3 ) { // Update additional Participant data
        $id = $this->args[ 2 ];
        switch ( $this->args[ 1 ] ) {
          case 'contact':
            $contact = $participant->get_contact( $id );

            $contact->setFirstName( $this->request[ 'first_name' ] );
            $contact->setLastName( $this->request[ 'last_name' ] );
            $contact->setRelation( $this->request[ 'relation' ] );
            $contact->setEmail( $this->request[ 'email' ] );
            $contact->setPhone( $this->request[ 'phone' ] );
            $contact->setAddress( $this->request[ 'address' ] );
            $contact->setAddressCity( $this->request[ 'address_city' ] );
            $contact->setAddressZip( $this->request[ 'address_zip' ] );
            $contact->setAddressState( $this->request[ 'address_state' ] );

            $_SESSION[ 'update_status' ] = $contact->update();
            header( "Location: /staff/dashboard/client/?client_id=" . $participant->getClientId() );
            die();
            break;
          case 'diet_restriction':
            $diet = $participant->get_diet_restriction( $id );

            $diet->setRestriction( $this->request[ 'restriction' ] );

            $_SESSION[ 'update_status' ] = $diet->update();

            header( "Location: /staff/dashboard/client/medical/?client_id=" . $participant->getClientId() );
            die();
            break;
          case 'emergency_contact':
            $contact = $participant->get_emergency_contact( $id );

            $contact->setFirstName( $this->request[ 'first_name' ] );
            $contact->setLastName( $this->request[ 'last_name' ] );
            $contact->setPhone( $this->request[ 'phone' ] );
            $contact->setAlternatePhone( $this->request[ 'alternate_phone' ] );

            $_SESSION[ 'update_status' ] = $contact->update();
            header( "Location: /staff/dashboard/client/emergency/?client_id=" . $participant->getClientId() );
            die();
            break;
          case 'medical_alert':
            $alert = $participant->get_medical_alert( $id );

            $alert->setAlert( $this->request[ 'alert' ] );

            $_SESSION[ 'update_status' ] = $alert->update();
            header( "Location: /staff/dashboard/client/medical/?client_id=" . $participant->getClientId() );
            die();
            break;
          case 'medication':
            $medication = $participant->get_medication( $id );

            $medication->setMedication( $this->request[ 'medication' ] );
            $medication->setDosage( $this->request[ 'dosage' ] );
            $medication->setFrequency( $this->request[ 'frequency' ] );
            $medication->setTimeTaken( $this->request[ 'time_taken' ] );

            $_SESSION[ 'update_status' ] = $medication->update();
            header( "Location: /staff/dashboard/client/medical/?client_id=" . $participant->getClientId() );
            die();
            break;
          case 'physical_limitation':
            $physical = $participant->get_physical_limitation( $id );

            $physical->setLimitation( $this->request[ 'limitation' ] );

            $_SESSION[ 'update_status' ] = $physical->update();
            header( "Location: /staff/dashboard/client/medical/?client_id=" . $participant->getClientId() );
            die();
            break;
          default:
            return "Invalid argument";
        }
      }
    } else {
      return "Unknown Request";
    }
  }

  /**
   * Request a subset of clients from the DB
   */
  protected function clients() {
    if ( $this->method == 'GET' ) {
      $list = array();
      $result = Client::query_all();

      foreach ( $result as $user ) {
        $list[ $user->getClientId() ] = $user->decode();
      }

      return $list;
    } else {
      return "Only accepts GET requests";
    }
  }

//  protected function account() {
//    if ( $this->method == 'GET' ) {
//      if ( empty( $this->args ) || $this->args[ 0 ] <= 0 )
//        return "Missing or Invalid Client ID";
//      $id = $this->args[ 0 ];  // Request Client ID
//      $account = Account::query_from_id( $id ); // Get Client from database
//
//      if ( count( $this->args ) == 1 ) { // return the client
//        return $account->decode();
//      } else if ( count( $this->args ) == 2 ) { // return array of client additional info
//        switch ( $this->args[ 1 ] ) {
//          case 'client':
//            $data = $account->get_client();
//            break;
//          default:
//            return "Invalid argument";
//        }
//        return $data->decode();
//      } else {
//        return "Invalid number of arguments";
//      }
//    } else if ( $this->method == 'POST' ) {
//      return "POST To Be Added!";
//    } else {
//      return "Unknown Request";
//    }
//  }

  /**
   * Request a subset of clients from the DB
   */
//  protected function accounts() {
//    if ( $this->method == 'GET' ) {
//      $list = array();
//      $result = Account::query_all();
//
//      foreach ( $result as $user ) {
//        $list[ $user->getAccountID() ] = $user->decode();
//      }
//
//      return $list;
//    } else {
//      return "Only accepts GET requests";
//    }
//  }

  /**
   * Request a subset of clients from the DB
   */
  protected function accounts_joined() {
    if ( $this->method == 'GET' ) {
      $list = array();
      $result = array();

      $offset = isset( $this->request[ 'offset' ] ) && is_numeric( $this->request[ 'offset' ] ) ? $this->request[ 'offset' ] : 0;
      $search = isset( $this->request[ 'search' ] ) ? $this->request[ 'search' ] : "";

      $result = Participant::query_all( $offset, $search );

      foreach ( $result as $user ) {
        $list[ $user->getAccountID() ] = $user->decode();
      }
      $results[ 'data' ] = array_values( $list );

      return $results;
    } else {
      return "Only accepts GET requests";
    }
  }

  /**
   * Request all participants
   */
//  protected function data_table() {
//    if ( $this->method == 'GET' ) {
//      $list = array();
//
//      $result = Participant::query_for_data_table();
//
//      foreach ( $result as $user ) {
//        $list[ $user->getClientId() ] = $user->decode();
//      }
//      $results['data'] = array_values($list);
//
//      return $results;
//    } else {
//      return "Only accepts GET requests";
//    }
//  }
}