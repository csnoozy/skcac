<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Check Email</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono" rel="stylesheet">
    <style>
        body{
            font-family: 'Ubuntu Mono', monospace;
            font-size: 20px;
        }
    </style>
</head>
<body>
    <!--logo goes here-->
    <div class="container">
        <!--Header section here-->
        <div class="text-center">
            <br>
            <img src="http://aabdalla.greenriverdev.com/TeamSprintbacklog2/skcac%20header.png" alt="header">
        </div>
    </div>
<div class="container">
        <!--table goes here with message and colors-->
            <nav class="navbar bg-default">
                <ul class="navbar-nav">
                    <li>
                        <br>
                        <a class="nav-link navbar-brand text-uppercase text-black" style="font-size: 40px;"><strong>Check your email to change your password</strong></a>
                        <br>
                    </li>
                </ul>
            </nav>
    <form method="post" action="../LandingPages/NewPassword.php">
        <button type="submit" id="submit" class="btn btn-warning btn-lg  text-uppercase">next page for demonstration only</button>
    </form>
</div>
<!--footer starts here-->
<div>
    <br>
    <!--footer goes here-->
    <table class="table table-info">
        <tbody>
        <tr>
            <th></th>
        </tr>
        <tr>
            <th scope="row"> SKCAC Industries and Employment Services</th>
        </tr>
        <tr>
            <th>Phone: 253-395-1240   TDD RELAY: 711</th>
        </tr>
        <tr>
            <th>19731 Russell Road South, Kent, Washington 98032-1117</th>
        </tr>
        <tr>
            <th> </th>
        </tr>
        <tr>
            <th> </th>
        </tr>
        </tbody>
    </table>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>