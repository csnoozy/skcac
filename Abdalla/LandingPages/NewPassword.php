<?php
/*
// Check for form submission:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require '../common/Account.php'; //Connect to Account
    $errors = []; // Initialize an error array.
    // new password
    if (empty($_POST['password'])) {
        $errors[] = 'You forgot to enter your new password.';
    } else {
        $password = mysqli_real_escape_string($dbc, trim($_POST['password']));
    }
    // Confirm password
    if (empty($_POST['confirm password'])) {
        $errors[] = 'You forgot to enter your confirm password.';
    } else {
        $password = mysqli_real_escape_string($dbc, trim($_POST['confirm password']));
    }
}
$account = Account::query_from_email( $submitted_password );
if ( !empty( $_POST) ) {
$submitted_email = $_POST[ 'password' ];
$confirm_email = $_POST[ 'confirm password' ];

// check if password matches
    if (($submitted_password) === ($confirm_email)) {
        // update in the database with new password
        $q = "INSERT INTO Account (password) VALUES ('$submitted_password')";
        $r = @mysqli_query($dbc, $q); // Run the query.
        if ($r) {
            // If it ran OK.
            // go back to log in page
            require('../LandingPages/signIn.php');
            echo file_get_contents('signIn.php');
        }
    }
}
*/?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono" rel="stylesheet">
    <style>
        body{
            font-family: 'Ubuntu Mono', monospace;
            font-size: 20px;
        }
    </style>
</head>

<body>
    <!--logo goes here-->
    <div class="container">
        <!--Header section here-->
        <div class="text-center">
            <br>
            <a href="index.php">
                <img src="http://aabdalla.greenriverdev.com/TeamSprintbacklog2/skcac%20header.png" alt="header">
            </a>

        </div>
    </div>

<div class="container">
        <!--table goes here with message and colors-->
        <div>
            <nav class="navbar bg-default">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link navbar-brand text-uppercase text-black" style="font-size: 45px; ">New Password</a>
                    </li>
                </ul>
            </nav>
        </div>


        <div class="container">
            <form class="text-uppercase" method="post" action="../LandingPages/index.php">
                <div class="form-group text-uppercase">
                    <br>
                    <input type="password" class="form-control" id="newpassword" placeholder="NEW PASSWORD*" required>
                    <div class="invalid-feedback"> Invalid email or password </div>
                </div>
                <div class="form-group text-uppercase">
                    <br>
                    <input type="password" class="form-control" id="confirmPassword" placeholder="CONFIRM PASSWORD*" required>
                </div>
                <br>
                <!--button goes to sign in page-->
                <button type="submit" id="submit" class="btn btn-success btn-lg btn-block text-uppercase">Finish</button>
            </form>
            <br>
        </div>

</div>
<!--footer starts here-->

    <!--footer goes here-->
    <table class="table table-info">
        <tbody>
        <tr>
            <th></th>
        </tr>
        <tr>
            <th scope="row"> SKCAC Industries and Employment Services</th>
        </tr>
        <tr>
            <th>Phone: 253-395-1240   TDD RELAY: 711</th>
        </tr>
        <tr>
            <th>19731 Russell Road South, Kent, Washington 98032-1117</th>
        </tr>
        <tr>
            <th> </th>
        </tr>
        <tr>
            <th> </th>
        </tr>
        </tbody>
    </table>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
