<?php
/*// forgot password
// Show errors:
ini_set('display_errors', 1);
// Start session
/*if ( !empty( $_SESSION[ 'user' ] ) ) {
    header( "Location: ../dashboard" );
    die( "Redirecting to ../dashboard" );
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require '../common/index.php'; // Connect to index.
    require '../common/Account.php'; //Connect to Account
    require('../LandingPages/checkMail.php');

    if ( !empty( $_POST) ) {
        $submitted_email = $_POST[ 'email' ];

        $account = Account::query_from_email( $submitted_email );
        if ( !empty( $account ) ) {

            // check if email matches
            if ( $submitted_email === $account->getEmail() ) {
                $login_ok = true;
            }

            // if email matches move on to the next page
            if ( $login_ok ) {
                $_SESSION[ 'user' ] = $account->getEmail();
                $_SESSION[ 'staff' ] = $account->isStaff();
                $_SESSION[ 'admin' ] = $account->isAdmin();



                // to be changed with production dashboard/ official dashboard
                header( "Location: ../LandingPages/checkMail.php" );
                die( "Redirecting to ../LandingPages/checkMail.php" );
            } else {
                printf( "Login Failed!" );
                $submitted_email = htmlentities( $_POST[ 'email' ], ENT_QUOTES, 'UTF-8' );
            }

        }
    }

}
*/?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forgot Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono" rel="stylesheet">
    <style>
        body{
            font-family: 'Ubuntu Mono', monospace;
            font-size: 20px;
        }
    </style>
</head>

<body>
<!--logo goes here-->
    <div class="container">
        <!--Header section here-->
        <div class="text-center">
            <br>
            <img src="http://aabdalla.greenriverdev.com/TeamSprintbacklog2/skcac%20header.png" alt="header">
        </div>
    </div>

<div class="container">
        <!--table goes here with message and colors-->
            <nav class="navbar bg-default">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link navbar-brand text-uppercase text-black" style="font-size: 45px; ">Forgot Password</a>
                    </li>
                </ul>
            </nav>

        <div class="container">
            <form class="text-uppercase" method="POST" action="../LandingPages/checkMail.php">
                <div class="form-group text-uppercase">
                    <br>
                    <input name="email" type="email" id="email" class="form-control is-valid" placeholder="EMAIL*" required>
                    <div class="invalid-feedback"> Invalid email or password </div>
                </div>
                <button type="submit" value="continue" class="btn btn-success btn-lg btn-block text-uppercase">Continue
                
                    <!-- send email to the user-->
                   <!-- --><?php
/*                    // the message
                    $msg = "You have requested a password change \nClick here to reset your password\n";

                    // use wordwrap() if lines are longer than 70 characters
                    $msg = wordwrap($msg,70);

                    // send email
                    mail("$submitted_email","SKCAC Password change",$msg);
                    */?>
                </button>
            </form>
            <br>
        </div>
</div>
<!--footer starts here-->
<div>
    <!--footer goes here-->
    <table class="table table-info">
        <tbody>
        <tr>
            <th></th>
        </tr>
        <tr>
            <th scope="row"> SKCAC Industries and Employment Services</th>
        </tr>
        <tr>
            <th>Phone: 253-395-1240   TDD RELAY: 711</th>
        </tr>
        <tr>
            <th>19731 Russell Road South, Kent, Washington 98032-1117</th>
        </tr>
        <tr>
            <th> </th>
        </tr>
        <tr>
            <th> </th>
        </tr>
        </tbody>
    </table>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>